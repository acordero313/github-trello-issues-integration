import requests
import json

# User must generate their own unique API_KEY, OAUTH_TOKEN, and API_TOKEN,
# follow link for instructions on generating key.
API_KEY = '{ YOUR API KEY }'
OAUTH_TOKEN = '{ YOUR OAUTH TOKEN }'
API_TOKEN = '{ YOUR API TOKEN }'

# Board ID can be retrieved by accessing the board URl + ".json"
BOARD_ID = '{ YOUR BOARD ID }'

# List ID can be retrieved by accessing the list URL + ".json"
TO_DO_LIST_ID = '{ YOUR LIST ID }'

# List ID can be retrieved by accessing the list URL + ".json"
LIST_ID = {
    'p0_list_id': '{ YOUR LIST ID }',
    'p1_list_id': '{ YOUR LIST ID }',
    'p2_list_id': '{ YOUR LIST ID }',
    'p3_list_id': '{ YOUR LIST ID }',
    'p4_list_id': '{ YOUR LIST ID }'
}


class TrelloRequests:

    @staticmethod
    def card_post_request(url, issues_list):
        """
        Aggregates all GitHub issues into Trello board.

        :param url: Trello url to make https request to
        :param issues_list: List of unique GitHub repository issues
        """
        for issue_info in issues_list:

            new_title = issue_info['repository'] + ":" + str(issue_info['number']) + ":" + issue_info['title']

            date = "Date created: " + issue_info['created_at']

            # Every issue will initially be set to "To Do" list
            querystring = {'idList': TO_DO_LIST_ID, "name": new_title,
                           "desc": date, "keepFromSource": "all",
                           "urlSource": issue_info['url'], "key": API_KEY, "token": API_TOKEN}

            p = requests.post(url, data=querystring)

            print(p.text)

    @staticmethod
    def get_card_update_request():
        """
        Makes Trello request to get updated prioritization for GitHub issues.

        :return: Unique list of GitHub issues and that have been updated/prioritized on the Trello board
        """
        payload = {"key": API_KEY, "token": API_TOKEN}
        cards = []

        for key in LIST_ID:
            result = requests.get('https://api.trello.com/1/lists/' +
                                  LIST_ID.get(key) + '/cards?fields=id,name,badges,labels', params=payload)

            print(LIST_ID.get(key))
            print(result.status_code)

            data = json.loads(result.text)

            for card_info in data:
                cards_data = dict()

                card_name = card_info['name']

                parsed_info = card_name.split(':')

                repo = parsed_info[0]
                issue_num = parsed_info[1]
                label = key

                cards_data.update({'repository': repo})
                cards_data.update({'number': issue_num})
                cards_data.update({'label': label})

                cards.append(cards_data)

        return cards
