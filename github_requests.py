import requests
import json

# Must use unique user API key, follow link to learn about how to generate key
# https://developer.github.com/v3/auth/#basic-authentication
GITHUB_API_KEY = '{ YOUR API KEY }'


class GithubRequest:

    @staticmethod
    def issues_get_request(url, repo):
        """
        Requests and aggregates GitHub repository issues.

        :param url: GitHub url to make https request to
        :param repo: Name of Github repository

        :return: List of unique GitHub issues with their corresponding information, specific to a repository
        """
        res = requests.get(url)

        print(res.status_code)

        data = json.loads(res.text)

        issues = []
        for issues_info in data:
            issues_data = dict()

            issue_url = issues_info['url']
            title = issues_info['title']
            created_at = issues_info['created_at']
            issue_number = issues_info['number']

            issues_data.update({'url': issue_url})
            issues_data.update({'title': title})
            issues_data.update({'created_at': created_at})
            issues_data.update({'number': issue_number})
            issues_data.update({'repository': repo})

            issues.append(issues_data)

        return issues

    @staticmethod
    def update_github_label(updated_trello_cards):
        """
        Updates GitHub issues with TrelloRequests prioritization.

        :param updated_trello_cards: List of individual updated TrelloRequests
        """
        user = '{ YOUR GITHUB USER NAME }'

        for i in updated_trello_cards:

            repository = i['repository']
            issue_num = i['number']
            label = i['label']

            payload = {'labels': [label]}

            login = requests.patch('https://api.github.com/repos/USERNAME/' + repository
                                   + '/issues/' + issue_num, auth=(user, GITHUB_API_KEY), data=json.dumps(payload))
            print(login.status_code)
