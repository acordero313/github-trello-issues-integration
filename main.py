from github_requests import GithubRequest
from trello_request import TrelloRequests

URL_TRELLO_CARDS = 'https://api.trello.com/1/cards'
URL_TRELLO_GET_LIST = 'https://api.trello.com/1/lists/'

testing_issues_list = GithubRequest.issues_get_request('https://api.github.com/repos/acordero08/testing-integration/issues'
                                                       , 'testing-integration')

testing_issues_list2 = GithubRequest.issues_get_request('https://api.github.com/repos/acordero08/testing-integration2/issues'
                                                       , 'testing-integration2')

TrelloRequests.card_post_request(URL_TRELLO_CARDS, testing_issues_list)
TrelloRequests.card_post_request(URL_TRELLO_CARDS, testing_issues_list2)
updated_trello_cards = TrelloRequests.get_card_update_request()
GithubRequest.update_github_label(updated_trello_cards)

